# Examen 2019-2020
Bonjour, 
Ceci est l'�nonc� du sujet d'examen Java EE, ann�e 2019-2020.
Dans cet exeman il sera demand� � l'�tudiant de mettre en place le mod�le de donn�es d'une application des gestion de projet pour une grande entr�prise.
Ce mod�le est repr�sent� par le diagramme UML suivants
![Kiku](src/main/resources/uml/image.jpg) 

Ce diagramme UML est aussi disponible dans le r�pertoire exam2020 /src /main/resources/ classesdiagrams.mdj . Pour l�ouvrir il vous faut installer le logiciel de mod�lisation UML (exemple startUML).

Les classes java sont d�j� cr��es et disponible dans le projet � l'url (https://gitlab.com/upec2019/exam2020.git).

#Decription du projet
## Code source
Les sources du projet sont disponible dans le repertoire gitlab (https://gitlab.com/upec2019/exam2020.git). Pour r�cup�rer les sources il vous faudra un client git. Le repository n'est pas prot�g� par mot de passe.

Faire une commande  `git clone https://gitlab.com/upec2019/exam2020.git exam2020` pour cloner le projet. 

C'est un projet maven compilable avec la commande `mvn clean install` 
Le projet est pr�configur� pour utiliser une base de donn�es H2 embarqu�e. Pas besoin d'installer une autre base de donn�s pour faire vos tests.

La structure des repertoires 
![Kiku](src/main/resources/uml/classes.jpg)
## Diagramme UML et relations

Toutes les relations entre les classes sont exprim�es dans le diagramme UML ainsi que dans code fourni.  

Le diagramme contient une relation unidirectionnelle (`Employee -> Address`), quatre relations bidirectionnelles (`Employee <-> Phone`, `Employee <-> Project`, `Employee <-> Dept` et `Employee <-> Employee`),   et quatres h�ritages

## Les packages

1. Le package `edu.upec.m2.model`  contient l'ensemble des classes du mod�le de l'aplications. Ceux la base des entit�s qui vous sera demand� d'annoter correctement.  

2. Le package `edu.upec.m2.jpq` contient l'interface que ti devez impl�menter d�finissant les r�qu�tes JPQL demand�es plus base.

3. Le package `edu.upec.m2.client` contient les classes utilitaires suivants qui permettent de tester vos relations

	EntityInserter : permet de remplir la base de donn�es donn�es de simulations

	H2Viewer : pour visualiser la base de donn�es H2 dans un browser

	QueryTester : C�est un ex�cuteur de requ�te JPQL afin de faciliter vos tests. Une fois lancer, elle lit les requ�tes pass�es dans la console, les ex�cute et affiche le r�sultat. 

# Questions
## Les entit�s

Annoter les classes selon les consignes suivantes : 
 
*	les classes abstract doivent �tre annot�es MappedSuperClass

*	les noms des employ�s doivent avoir au moins 2 caract�res. Utiliser les m�thodes callback pour v�rifier cette contrainte et emp�cher l�enregistrement /mise � jours de l�entit� si cette contrainte n�est pas respect�e

*	les cl�s primaires doivent �tre g�n�r�es par la m�me s�quence de nom EXAM2020_SEQUENCE commen�ant � 1 et allou�e par pas de 50

*	les h�ritages doivent �tre de type SINGLE_TABLE

* 	les tables

	�	EMP (entities Employee)
	
![Kiku](src/main/resources/uml/EMP.jpg)	

	�	ADDRESS
	
![Kiku](src/main/resources/uml/ADDRESS.jpg)

	�	DEPT
	
![Kiku](src/main/resources/uml/DEPT.jpg)

	�	PHONE
	
![Kiku](src/main/resources/uml/PHONE.jpg)

	�	EMP_PROJECT
	
![Kiku](src/main/resources/uml/EMP_PROJECT.jpg)
	
	�	SEQUENCE
	
![Kiku](src/main/resources/uml/SEQUENCE.jpg)


## Requ�te JPQL
 
Ecrite les requ�tes JPQL suivantes : 
*	Qui renvoie la liste des objets de type Phone et dont le type est OFFICE ou HOME

	```REPONSE```

*	Qui renvoie seulement les colonnes ID, NAME, SALARY MANAGER_ID, DEPT_ID et ADDRESS_ID de l�ensemble des  employ�s de la base de donn�es

	```REPONSE```

*	Qui renvoie, sans doublon, la liste des d�partements ayant au moins un employ�

	```REPONSE```

*	Qui renvoie le nom des  projets pour lesquels aucun employ� ne travaille

	```REPONSE```

*	Qui, en utilisant l�op�rateur JOIN, affiche les noms des employ�s avec leurs num�ros de t�l�phone

	```REPONSE```


*	Qui affiche la liste des couples (nom de l�employ�,  d�partement de rattachement) des employ�s qui sont manager (collaborators  NON VIDE)

	```REPONSE```


*	Qui affiche la liste des employ�s dont le salaire est compris entre 30000 et 70000

	```REPONSE```

*	Qui affiche la liste des d�partements dont le nom commence par �IT-�

	```REPONSE```

*	Qui affiche la liste des employ�s qui ont le salaire le plus �lev� (Attention, ce salaire ne doit pas figurer dans le requ�te, ni pass� en param�tre) .  Vous pouvez utiliser l�operateur MAX.

	```REPONSE```

*	Qui affiche le couple (d�partement, salaire moyen des employ�s)

	```REPONSE```

*	Qui affiche la liste des employ�s qui travaillent sur plus de 2 projets

	```REPONSE```


## Impl�menter l�interface 

`edu.upec.m2.jpql.QueryService`

Lire les commentaire javadoc de chaque m�thode pour savoir l'imppl�mentaton demand�. Ces m�thodes doivent �tre impl�ment�s sans utilisation de boucle java




 