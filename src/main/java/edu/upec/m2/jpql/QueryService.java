package edu.upec.m2.jpql;

import java.util.List;

import edu.upec.m2.model.Dept;
import edu.upec.m2.model.Employee;

public interface QueryService {
	/**Retourne le salaire de l'employ� dont le nom et le departement est pass� en param�tre
	 * 
	 * @param deptName nom du departement
	 * @param empName  nom de l'employ�
	 * @return
	 */
	long queryEmplSalary(String deptName, String empName);
	/**Retourne la list des employ�s du departement <code>dept</code> dont le salaire est sup�rieur � <code>minSal</code>
	 * 
	 * @param dept departement de l'employ�
	 * @param minSal salaire minimum
	 * @return
	 */
	List<Employee> findEmployeesAboveSal(Dept dept, long minSal);
	/**Permet d'assigner tous les employ�s du departement <code>deptName</code> � un  seul manager <code>manager</code>
	 * 
	 * @param deptName nom du departement
	 * @param manager manager d'assignement
	 */
	void assignManager(String deptName, Employee manager);
	/**
	 * Permet de supprimer tous les projets sur lesquels aucun employ�s ne travaille <code>employee.</code>
	 * 
	 */
	void removeEmtyProjects() ;
	/**
	 * 
	 * @return retourne une statatistique des salaire (en utilisant les op�rateur COUNT et AVG)
	 */
	List<EmployeeStat> getEmployeeStat();
	
}
