package edu.upec.m2.jpql;
/**Statistique : salaire moyen par departement
 * 
 * @author s4665982
 *
 */
public class EmployeeStat {
	private String deptName; //nom du departement
	private long avgSal;     //salaire moyen
	private int empNumber;   // nombre d'emmploy�s du departement
	public EmployeeStat(String deptName, long avgSal, int empNumber) {
		super();
		this.deptName = deptName;
		this.avgSal = avgSal;
		this.empNumber = empNumber;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public long getAvgSal() {
		return avgSal;
	}
	public void setAvgSal(long avgSal) {
		this.avgSal = avgSal;
	}
	public int getEmpNumber() {
		return empNumber;
	}
	public void setEmpNumber(int empNumber) {
		this.empNumber = empNumber;
	}
	
	
}
