package edu.upec.m2.model;

import java.util.List;

public class Dept extends BaseEntity{
	private String name;
	private List<Employee> employees;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	
	
	
}
