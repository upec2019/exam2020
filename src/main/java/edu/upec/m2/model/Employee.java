package edu.upec.m2.model;

import java.time.LocalDate;
import java.util.List;


public abstract class Employee extends  BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	private LocalDate startDate;
	private byte[] photo;
	private char[] comments;
	private Employee manager;
	private List<Employee> collaborators;
	private List<Phone> phones;
	private Dept department;
	private Address address;
	private List<Project> project;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public byte[] getPhoto() {
		return photo;
	}
	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}
	public char[] getComments() {
		return comments;
	}
	public void setComments(char[] comments) {
		this.comments = comments;
	}
	public Employee getManager() {
		return manager;
	}
	public void setManager(Employee manager) {
		this.manager = manager;
	}
	public List<Employee> getCollaborators() {
		return collaborators;
	}
	public void setCollaborators(List<Employee> collaborators) {
		this.collaborators = collaborators;
	}
	public List<Phone> getPhones() {
		return phones;
	}
	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}
	public Dept getDepartment() {
		return department;
	}
	public void setDepartment(Dept department) {
		this.department = department;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public List<Project> getProject() {
		return project;
	}
	public void setProject(List<Project> project) {
		this.project = project;
	}
	

}
