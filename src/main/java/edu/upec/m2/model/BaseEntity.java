package edu.upec.m2.model;

import java.io.Serializable;

public abstract class BaseEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
   public boolean isNew() {
        return this.id == null;
    }


	
}
