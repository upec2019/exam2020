package edu.upec.m2.client;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import edu.upec.m2.model.Address;
import edu.upec.m2.model.ContractEmployee;
import edu.upec.m2.model.Dept;
import edu.upec.m2.model.FullTimeEmployee;
import edu.upec.m2.model.PartTimeEmployee;
import edu.upec.m2.model.Phone;
import edu.upec.m2.model.PhoneType;
import edu.upec.m2.model.Project;

public class EntityInserter {

	public static void main(String[] args) {
		EntityManager em = Persistence.createEntityManagerFactory("exam2020").createEntityManager();
		try {
			em.getTransaction().begin();
			createUsers(em);
			em.getTransaction().commit();
		}catch(Exception ex) {
			ex.printStackTrace();
			em.getTransaction().rollback();
			
		}finally {
			em.close();
			em.getEntityManagerFactory().close();
		}
	}
	public static void createUsers(EntityManager em) {

		Dept sale = new Dept();
		sale.setName("SALE");
		em.persist(sale);
		Dept itrisk = new Dept();
		itrisk.setName("IT-RISK");
		em.persist(itrisk);
		Dept itdev = new Dept();
		itdev.setName("IT-ETUDE & DEV");
		em.persist(itdev);		
		
		Dept accounting = new Dept();
		accounting.setName(" ACCOUNTING");
		em.persist(accounting);
		Dept security = new Dept();
		security.setName("SECURITY");
		em.persist(security);
		
		
		ContractEmployee john  = new ContractEmployee();
		john.setStartDate(LocalDate.of(2006, 11, 20));
		john.setName("John");
		john.setDailyRate(500);
		john.setTerm(12);
		em.persist(john);
		Address johnaddr  = new Address();
		johnaddr.setStreet("2 rue A");
		johnaddr.setCity("Paris");
		johnaddr.setZipCode("75013");
		em.persist(johnaddr);
		john.setAddress(johnaddr);
		Phone johnphone = new Phone();
		johnphone.setNumber("0623456754");
		johnphone.setType(PhoneType.MODBILE);
		johnphone.setEmployees(john);
		em.persist(johnphone);
		john.setPhones(Arrays.asList(johnphone));
		john.setDepartment(sale);
				
		
		ContractEmployee paul  = new ContractEmployee();
		paul.setStartDate(LocalDate.of(2016, 7, 23));
		paul.setName("Paul");
		paul.setDailyRate(600);
		paul.setTerm(24);
		em.persist(paul);
		Address pauladdr  = new Address();
		pauladdr.setStreet("2 rue A");
		pauladdr.setCity("Paris");
		pauladdr.setZipCode("75013");
		em.persist(pauladdr);
		paul.setAddress(pauladdr);		
		Phone paulphone = new Phone();
		paulphone.setNumber("0123456754");
		paulphone.setType(PhoneType.HOME);
		paulphone.setEmployees(john);
		em.persist(paulphone);
		paul.setPhones(Arrays.asList(paulphone));		
		
		
		ContractEmployee sarah  = new ContractEmployee();
		sarah.setStartDate(LocalDate.of(2012, 1, 15));
		sarah.setName("Sarah");
		sarah.setDailyRate(700);
		sarah.setTerm(18);
		em.persist(sarah);	
		Address sarahaddr  = new Address();
		sarahaddr.setStreet("2 rue A");
		sarahaddr.setCity("Paris");
		sarahaddr.setZipCode("75013");
		em.persist(sarahaddr);
		sarah.setAddress(sarahaddr);		

		Phone sarahphone = new Phone();
		sarahphone.setNumber("0123456754");
		sarahphone.setType(PhoneType.OFFICE);
		sarahphone.setEmployees(sarah);
		em.persist(sarahphone);
		sarah.setPhones(Arrays.asList(sarahphone));

		
		PartTimeEmployee jackie = new PartTimeEmployee();
		jackie.setName("Jackie");
		jackie.setStartDate(LocalDate.of(2007, 11, 20));
		jackie.setVacation(17);
		jackie.setHourlyRate(15);
		em.persist(jackie);
		Address jackieaddr  = new Address();
		jackieaddr.setStreet("2 rue A");
		jackieaddr.setCity("Orly");
		jackieaddr.setZipCode("94310");
		em.persist(jackieaddr);
		jackie.setAddress(jackieaddr);		
		
		
		
		PartTimeEmployee ryan = new PartTimeEmployee();
		ryan.setName("Ryan");
		ryan.setStartDate(LocalDate.of(2016, 03, 01));
		ryan.setVacation(15);
		ryan.setHourlyRate(17);	
		em.persist(ryan);
		Address ryanaddr  = new Address();
		ryanaddr.setStreet("2 rue A");
		ryanaddr.setCity("Orly");
		ryanaddr.setZipCode("94310");
		em.persist(ryanaddr);
		ryan.setAddress(ryanaddr);		
		
		
		PartTimeEmployee mark = new PartTimeEmployee();
		mark.setName("Mark");
		mark.setStartDate(LocalDate.of(2006, 11, 20));
		mark.setVacation(15);
		mark.setHourlyRate(17);
		em.persist(mark);
		Address markaddr  = new Address();
		markaddr.setStreet("2 rue A");
		markaddr.setCity("Orly");
		markaddr.setZipCode("94310");
		em.persist(markaddr);
		mark.setAddress(markaddr);		
		
		
		FullTimeEmployee patrick  = new FullTimeEmployee();
		patrick.setStartDate(LocalDate.of(2010, 9, 12));
		patrick.setName("Patrick");
		patrick.setPension(100000);
		patrick.setSalary(55000);
		patrick.setVacation(15);
		em.persist(patrick);
		Address patrickaddr  = new Address();
		patrickaddr.setStreet("2 rue A");
		patrickaddr.setCity("Cr�teil");
		patrickaddr.setZipCode("94000");
		em.persist(patrickaddr);
		patrick.setAddress(patrickaddr);		
		
		
		FullTimeEmployee hoan  = new FullTimeEmployee();
		hoan.setStartDate(LocalDate.of(2008, 12, 20));
		hoan.setName("Hoan");
		hoan.setPension(110000);
		hoan.setSalary(59000);
		hoan.setVacation(20);
		em.persist(hoan);
		Address hoanaddr  = new Address();
		hoanaddr.setStreet("2 rue A");
		hoanaddr.setCity("Cr�teil");
		hoanaddr.setZipCode("94000");
		em.persist(hoanaddr);
		hoan.setAddress(hoanaddr);		
		
		FullTimeEmployee sam  = new FullTimeEmployee();
		sam.setStartDate(LocalDate.of(2006, 11, 20));
		sam.setName("Sam");
		sam.setPension(445000);
		sam.setSalary(60000);
		sam.setVacation(10);	
		em.persist(sam);
		Address samaddr  = new Address();
		samaddr.setStreet("2 rue A");
		samaddr.setCity("Cr�teil");
		samaddr.setZipCode("94000");
		em.persist(samaddr);
		sam.setAddress(samaddr);		
		
		
		sam.setCollaborators(Arrays.asList(hoan, patrick, mark, ryan, jackie, sarah, paul, john));
		hoan.setManager(sam);
		patrick.setManager(sam);
		mark.setManager(sam);
		ryan.setManager(sam);
		jackie.setManager(sam);
		sarah.setManager(sam);
		paul.setManager(sam);
		john.setManager(sam);

		Project project = new Project();
		project.setName("SMS plateform");
		project.setStartDate(new Date());	
		em.persist(project);
		project.setEmployee(Arrays.asList(patrick, mark, ryan, jackie, sarah, paul));
		
		patrick.setProject(Arrays.asList(project));
		mark.setProject(Arrays.asList(project));
		ryan.setProject(Arrays.asList(project));
		jackie.setProject(Arrays.asList(project));
		sarah.setProject(Arrays.asList(project));
		paul.setProject(Arrays.asList(project));
		
		sam.setDepartment(itrisk);
		hoan.setDepartment(itrisk);
		patrick.setDepartment(itrisk);
		mark.setDepartment(itrisk);
		ryan.setDepartment(itrisk);
		jackie.setDepartment(itrisk);
		sarah.setDepartment(itrisk);
		paul.setDepartment(itrisk);
		john.setDepartment(itrisk);
		itrisk.setEmployees(Arrays.asList(hoan, patrick, mark, ryan, jackie, sarah, paul, john));
		
	}

}
