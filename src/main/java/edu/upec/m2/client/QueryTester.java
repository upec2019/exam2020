package edu.upec.m2.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class QueryTester {
	private static final String UNIT_NAME = "exam2020";

	public static void main(String[] args) throws Exception {
		Map<String, String > prop = new HashMap<>();
		prop.put("hibernate.hbm2ddl.auto", "none");
		EntityManager em = Persistence.createEntityManagerFactory(UNIT_NAME, prop).createEntityManager();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			for(;;) {
				System.out.print("JPQL> ");
				String query = reader.readLine();
				if("exit".equalsIgnoreCase(query)) {
					break;
				}
				if(query==null || query.length()==0) {
					continue;
				}
				try {
					List<?> result = em.createQuery(query).getResultList();
					if(result!=null && result.size()>0) {
						int count = 0;
						for(Object o : result) {
							System.out.print(++count+". ");
							printResult(o);
						}
					}
				}catch(Exception ex) {
					
				}
			}
		}finally {
			em.close();
			em.getEntityManagerFactory().close();
		}
	}
	private static void printResult(Object result) {
		if(result==null) {
			System.out.println("NULL");
		}else if(result instanceof Object[]) {
			Object[] row = (Object[])result;
			System.out.println("[");
			for(Object o : row) {
				printResult(o);
			}
			System.out.println("]");
		} else if (result instanceof Double || result instanceof Long || result instanceof Integer || result instanceof Float || result instanceof String) {
			System.out.println(result.getClass().getName()+" : "+result);
		}else {
			System.out.println(ReflectionToStringBuilder.toString(result, ToStringStyle.SHORT_PREFIX_STYLE));
		}
		System.out.println();
	}
}
